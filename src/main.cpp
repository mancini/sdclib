//
// Created by mancini on 4-4-23.
//
#include <iostream>
#include <emscripten/emscripten.h>
#ifdef __cplusplus
#define EXTERN extern "C"
#else
#define EXTERN
#endif

EXTERN EMSCRIPTEN_KEEPALIVE bool isEven(int n){
    if(n%2==0) return true;
    return false;
}

int main() {
    std::cout<<"Hello world"<<std::endl;

    return 0;
}